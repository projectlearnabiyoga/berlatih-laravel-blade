<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('judul', 45);
            $table->string('isi', 255);
            // $table->bigInteger('jawaban_tepat_id')->unsigned();
            $table->bigInteger('jawaban_tepat_id');
            $table->bigInteger('profil_id')->unsigned();
            $table->foreign('profil_id')
                ->references('id')
                ->on('profil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaan');
    }
}