<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawaban', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('isi', 255);
            $table->bigInteger('pertanyaan_id')->unsigned();
            $table->bigInteger('profil_id')->unsigned();
            $table->foreign('profil_id')
                ->references('id')
                ->on('profil');
            $table->foreign('pertanyaan_id')
                ->references('id')
                ->on('pertanyaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jawaban');
    }
}