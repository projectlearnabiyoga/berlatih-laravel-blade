@extends('welcome')
@section('Menu', 'Pertanyaan')
@section('Konten')
<div class="card">
    <div class="card-body">
        <div class="clearfix">
            <a href="{{'pertanyaan/create'}}" style="decoration:none;">
                <button type="button" class="btn btn-primary btn-md mt-1 mb-4"">
                    <i class=" fa fa-plus"></i>
                    Ajukan Pertanyaan
                </button>
            </a>
        </div>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Pertanyaan</th>
                    <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php
                $no = 1;
                @endphp
                @foreach ($pertanyaan as $tanya)
                <tr>
                    <td class="text-center" width="3%">{{$no++}}</td>
                    <td>{{$tanya->judul}}</td>
                    <td>{{$tanya->isi}}</td>
                    <td class="text-center" width="15%">
                        <a href="/pertanyaan/{{$tanya->id}}" style="decoration:none;">
                            <button type="button" class="btn btn-info"><i class="fas fa-search"></i></button>
                        </a>
                        <a href="/pertanyaan/{{$tanya->id}}/edit" style="decoration:none;">
                            <button type="button" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></button>
                        </a>
                        <form class="mt-1" action="/pertanyaan/{{$tanya->id}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection