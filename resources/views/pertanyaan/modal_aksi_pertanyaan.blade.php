<div id="modal-edit-pertanyaan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    style="display: none;">
    <div class="modal-dialog modal-md">
        <form action="{{url('pertanyaan/create')}}" method="get">
            {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title" id="myLargeModalLabel">Ajukan Pertanyaan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Judul <span class="text-danger">*</span></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="judul_pertanyaan" require>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Pertanyaan <span class="text-danger">*</span></label>
                        <div class="col-md-9">
                            <textarea class="form-control" style="min-width: 100%" name="isi_pertanyaan"
                                id="isi_pertanyaan" rows="5" require></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left"
                        data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect text-left">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>