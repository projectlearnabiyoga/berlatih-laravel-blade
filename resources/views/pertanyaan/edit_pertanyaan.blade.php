@extends('welcome')
@section('Menu', 'Perbaharui Pertanyaan')
@section('Konten')
<div class="card">
    <div class="card-body">
        @foreach ($tampil as $det)
        <form action="/pertanyaan/{{$det->id}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label class="col-form-label col-md-3">Judul <span class="text-danger">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="judul_pertanyaan" value="{{$det->judul}}" require>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-md-3">Pertanyaan <span class="text-danger">*</span></label>
                <div class="col-md-9">
                    <textarea class="form-control" style="min-width: 100%" name="isi_pertanyaan" id="isi_pertanyaan"
                        rows="5" require>{{$det->isi}}</textarea>
                </div>
            </div>

            <br>
            <hr>
            <a href="{{'/pertanyaan'}}" style="decoration:none;">
                <button type="button" class="btn btn-danger waves-effect text-left">Close</button>
            </a>
            <button type="submit" class="btn btn-primary waves-effect text-left">Simpan</button>
        </form>
        @endforeach
    </div>
</div>
@endsection