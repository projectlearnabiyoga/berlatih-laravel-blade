@extends('welcome')
@section('Menu', 'Ajukan Pertanyaan')
@section('Konten')
<div class="card">
    <div class="card-body">
        <form action="{{url('pertanyaan')}}" method="post">
            {{csrf_field()}}
            <div class="form-group row">
                <label class="col-form-label col-md-3">Judul <span class="text-danger">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="judul_pertanyaan" require>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-md-3">Pertanyaan <span class="text-danger">*</span></label>
                <div class="col-md-9">
                    <textarea class="form-control" style="min-width: 100%" name="isi_pertanyaan" id="isi_pertanyaan"
                        rows="5" require></textarea>
                </div>
            </div>
            <br>
            <hr>
            <a href="{{'/pertanyaan'}}" style="decoration:none"><button type="button"
                    class="btn btn-danger waves-effect text-left">Close</button></a>
            <button type="submit" class="btn btn-primary waves-effect text-left">Simpan</button>
        </form>
    </div>
</div>
@endsection