@extends('welcome')
@section('Menu', 'Detail Pertanyaan')
@section('Konten')
<div class="card">
    <div class="card-body">
        @foreach ($detail as $det)
        <div class="form-group row">
            <label class="col-form-label col-md-3">Judul </label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="judul_pertanyaan" require value="{{$det->judul}}"
                    readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label col-md-3">Pertanyaan </label>
            <div class="col-md-9">
                <textarea class="form-control" style="min-width: 100%" name="isi_pertanyaan" id="isi_pertanyaan"
                    rows="5" require readonly>{{$det->isi}}</textarea>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection