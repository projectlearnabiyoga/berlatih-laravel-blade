<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;
use Session;

class PertanyaanController extends Controller
{
    //
    public function index(){
        $data['pertanyaan'] = Pertanyaan::all();
        return view('pertanyaan.pertanyaan', $data);
    }

    public function create(){
        return view('pertanyaan.tambah_pertanyaan');
    }

    public function store(Request $request){
        $pertanyaan = new Pertanyaan;
        $pertanyaan->judul = $request->judul_pertanyaan;
        $pertanyaan->isi = $request->isi_pertanyaan;
        $pertanyaan->jawaban_tepat_id = "1";
        $pertanyaan->profil_id = "1";
        if ($pertanyaan->save()) {
            Session::flash('message','Sukses');
            Session::flash('alert-data','Berhasil disimpan');
        } else {
            Session::flash('message','Gagal');
            Session::flash('alert-data','Gagal disimpan');
        }
        return redirect('pertanyaan');
    }

    public function show($id){
        $data['detail'] = Pertanyaan::where('id', $id)->get();
        return view('pertanyaan.show_pertanyaan', $data);
    }

    public function edit($id){
        $data['tampil'] = Pertanyaan::where('id', $id)->get();
        return view('pertanyaan.edit_pertanyaan', $data);
    }

    public function update($id_pertanyaan, Request $request){
        $query = Pertanyaan::where('id', $id_pertanyaan)
                    ->update([
                        'judul' => $request['judul_pertanyaan'],
                        'isi'   => $request['isi_pertanyaan']
                    ]);
        return redirect('pertanyaan');
    }

    public function destroy($id){
        $query = Pertanyaan::where('id', $id)->delete();
        return redirect('pertanyaan');
    }
}